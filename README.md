# Awapatent mail-template

## Content

1.  **template-as-dev.html**: template for awapatent A/S (css in the style-tag)
2.  **template-as.html**: template for awapatent A/S (css inline)

3.  **template-ab-dev.html**: mail template for awapatent AB (css in the style-tag)
4.  **template-ab.html** mail template for awapatent AB (css inline)

## Development

The mail templates are developed by editing the html and css in the template files (1 & 3).
The inline templates have been manually converted with an external tool ([Responsive Email CSS Inliner](https://htmlemail.io/inline/)) and checked for errors with the
[Responsive Email CSS Inliner](https://htmlemail.io/inline/).

## Production

The templates have to be adjusted by the client to populate data in correct places (paragraphs, headings etc). The img src have to be replaced with an url pointing to the image hosted by the customer.

## Test

The templates have been tested manually and automatic in some of the most popular OS and mail clients.

###The templates was ...

###... manually tested on Iphone 6s (iOs 11.3):

* [x] Outlook app
* [x] Gmail app
* [x] Built in mail app

###... manually tested on Ipad (iOs 11.3):

* [x] Outlook app
* [x] Gmail app
* [x] Built in mail app

###... manually tested on MacBook Pro (High Sierra 10.13.4)

* [x] Outlook for mac 16.11
* [x] Built in mail app
* [x] Gmail client in Google Chrome v65.0.3325.181
* [x] Gmail client in Firefox Quantum v59.0.2
* [x] Gmail client in Safari v11.1
* [x] Office365 client in Google Chrome v65.0.3325.181
* [x] Office365 client in Firefox Quantum v59.0.2
* [x] Office365 client in Safari v11.1
* [x] Outlook.com client in Google Chrome v65.0.3325.181
* [x] Outlook.com client in Firefox Quantum v59.0.2
* [x] Outlook.com client in Safari v11.1

###... manually tested on Windows 10:

* [x] Outlook 2016
* [x] Gmail client in Google Chrome v65.0.3325.181
* [x] Gmail client in Firefox Quantum v59.0.2
* [x] Gmail client in Microsoft Edge 41
* [x] Outlook.com client in Google Chrome v65.0.3325.181
* [x] Outlook.com client in Firefox Quantum v59.0.2
* [x] Outlook.com client in Microsoft Edge

###...automatically previewed in the following desktop clients ([Get Response Preview Tool](https://app.getresponse.com)):

* [x] Opera
* [x] Outlook 2010
* [x] Thunderbird 2
* [x] Thunderbird 3

## Further improvments

The css (style-css)-convertion could be done by grunt and should be implemented if the templates are to be changed more frequent or if more advanced content will be added.
